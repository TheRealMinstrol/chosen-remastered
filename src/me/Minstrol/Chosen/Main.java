package me.Minstrol.Chosen;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.*;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Main extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        Server server = Bukkit.getServer();
        ConsoleCommandSender console = server.getConsoleSender();
        Bukkit.getServer().getPluginManager().registerEvents(this, this);
        Bukkit.getServer().getPluginManager().registerEvents(new Chat(this), this);
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.getInventory().clear();
        }
        saveConfig();
        enableMethod();
    }

    Inventory inv;
    public Boolean started = false;
    public Boolean attempting2started = false;
    protected Connection connection;
    int playersNeeded = 2;
    int chosensDoubleJumps = 0;
    long diffInMillies;
    String prefix = ChatColor.GRAY + "[" + ChatColor.GREEN + "Chosen" + ChatColor.GRAY + "] " + ChatColor.RESET;
    ArrayList<String> chosen = new ArrayList<>();
    ArrayList<String> nonochosen = new ArrayList<>();
    ArrayList<String> cooldown = new ArrayList<>();
    ArrayList<String> jumpCooldown = new ArrayList<>();
    ArrayList<String> airstrikeCooldown = new ArrayList<>();
    ArrayList<String> shotgunCooldown = new ArrayList<>();


    @EventHandler
    public void playerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        PermissionUser pUser = PermissionsEx.getUser(player);
        player.setGameMode(GameMode.ADVENTURE);
        event.setJoinMessage(pUser.getPrefix() + player.getName() + ChatColor.YELLOW + " has joined chosen!");
        player.setFoodLevel(20);
        player.setHealth(20);
        player.setAllowFlight(false);
        player.getInventory().clear();
        if (Bukkit.getOnlinePlayers().size() >= playersNeeded) {
            if (!started) {
                if (!nonochosen.contains(player.getName())) {
                    nonochosen.add(player.getName());
                }
                Location spawnLoc = new Location(Bukkit.getWorld("world"), 2.5, 115, -30.5, 0, 0);
                player.teleport(spawnLoc);
                if (!attempting2started) {
                    attemptstart();
                    attempting2started = true;
                }
            } else {
                Location spawnLoc = new Location(Bukkit.getWorld("world"), 2.5, 115, -30.5, 0, 0);
                player.teleport(spawnLoc);
                nonochosen.add(player.getName());
                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5, 5);
                player.sendMessage(prefix + ChatColor.GOLD + "Please wait for the next game to start!");
            }
        } else {
            started = false;
            Location spawnLoc = new Location(Bukkit.getWorld("world"), 2.5, 115, -30.5, 0, 0);
            player.teleport(spawnLoc);
            nonochosen.add(player.getName());
            player.sendMessage(prefix + ChatColor.RED + "Not enough players to start game! (" + (playersNeeded - Bukkit.getOnlinePlayers().size()) + " required to start)");
        }
        try {
            PermissionUser permissionUser = PermissionsEx.getUser(player);
            Scoreboard board = Bukkit.getScoreboardManager().getMainScoreboard();
            Scoreboard sideBoard = Bukkit.getScoreboardManager().getNewScoreboard();
            Objective objective = sideBoard.registerNewObjective("test", "dummy");
            objective.setDisplaySlot(DisplaySlot.SIDEBAR);
            objective.setDisplayName(ChatColor.GREEN + "Chosen");
            Score score = objective.getScore(ChatColor.GREEN + "Coins:");
            score.setScore(getConfig().getInt("Users." + player.getName() + ".coins"));
            player.setScoreboard(board);
            Team color = board.getTeam(player.getName());
            if (color == null) {
                color = board.registerNewTeam(player.getName());
            }
            color.setPrefix(permissionUser.getPrefix());
            color.addPlayer(player);
            player.setScoreboard(board);
        } catch (NullPointerException localNullPointerException) {
            localNullPointerException.printStackTrace();
        }
    }

    public synchronized void attemptstart() {
        attempting2started = true;
        started = false;
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.getInventory().clear();
            player.sendMessage(prefix + ChatColor.GOLD + "Attempting to start game in 5 seconds...");
        }
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
            @Override
            public void run() {
                if (Bukkit.getOnlinePlayers().size() >= playersNeeded) {
                    if (!started) {
                        start();
                        attempting2started = false;
                    }
                } else {
                    started = false;
                    for (Player online : Bukkit.getOnlinePlayers()) {
                        online.sendMessage(" ");
                        online.sendMessage(prefix + ChatColor.RED + "Not enough players to start! Trying again again in 10 seconds!");
                        online.playSound(online.getLocation(), Sound.ENTITY_TNT_PRIMED, 5, 5);
                    }
                    Bukkit.getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("Chosen"), new Runnable() {
                        @Override
                        public void run() {
                            attemptstart();
                        }
                    }, 200L);
                }
            }
        }, 100L);
    }

    public void enableMethod() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (chosen.contains(player.getName())) {
                        double timeChosen = getDateDiff((Date) getConfig().get("Users." + player.getName() + ".started"), new Date(), TimeUnit.SECONDS);
                        if (timeChosen >= 360) {
                            player.setAllowFlight(false);
                            int coins = getConfig().getInt("Users." + player.getName() + ".coins");
                            getConfig().set("Users." + player.getName() + ".coins", coins + 120);
                            saveConfig();
                            player.sendMessage(" ");
                            player.sendMessage(ChatColor.BLUE + "" + ChatColor.BOLD + "-------------------------------------------");
                            player.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "                    Reward!");
                            player.sendMessage(ChatColor.GOLD + "You have been awarded " + 120 + " coins for playing a max time of 6 minutes!");
                            player.sendMessage(ChatColor.BLUE + "" + ChatColor.BOLD + "-------------------------------------------");
                            chosen.remove(player.getName());
                            Location spawnLoc = new Location(Bukkit.getWorld("world"), 2.5, 115, -30.5, 0, 0);
                            player.teleport(spawnLoc);
                            nonochosen.add(player.getName());
                            for (Player online : Bukkit.getOnlinePlayers()) {
                                online.sendMessage(prefix + ChatColor.GREEN + "The round has ended. New player will be chosen!");
                                online.getInventory().clear();
                            }
                            if (!attempting2started) {
                                attemptstart();
                                attempting2started = true;
                            }
                        }
                    }
                }
                if (Bukkit.getOnlinePlayers().size() > 1) {
                    if (chosen.isEmpty()) {
                        if (!attempting2started) {
                            attemptstart();
                        }
                    }
                }
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (player.getLocation().getY() <= 99) {
                        if (chosen.contains(player.getName())) {
                            player.setAllowFlight(false);
                            chosen.remove(player.getName());
                            Location spawnLoc = new Location(Bukkit.getWorld("world"), 2.5, 115, -30.5, 0, 0);
                            player.teleport(spawnLoc);
                            double timeChosen = getDateDiff((Date) getConfig().get("Users." + player.getName() + ".started"), new Date(), TimeUnit.SECONDS);
                            if (timeChosen >= 360) {
                                awardcoins(120, player);
                            } else if ((timeChosen > 180) && (timeChosen < 240)) {
                                awardcoins(75, player);
                            } else if ((timeChosen > 120) && (timeChosen < 180)) {
                                awardcoins(50, player);
                            } else if ((timeChosen > 60) && (timeChosen < 120)) {
                                awardcoins(25, player);
                            } else if ((timeChosen > 30) && (timeChosen < 60)) {
                                awardcoins(10, player);
                            } else if ((timeChosen > 0) && (timeChosen < 30)) {
                                awardcoins(5, player);
                            }
                            nonochosen.add(player.getName());
                            for (Player online : Bukkit.getOnlinePlayers()) {
                                online.sendMessage(prefix + ChatColor.GREEN + "The round has ended. New player will be chosen!");
                                online.getInventory().clear();
                            }
                            if (!attempting2started) {
                                attemptstart();
                                attempting2started = true;
                            }
                        }
                    }
                }
            }
        }, 0L, 1L);
    }

    @EventHandler
    public void setFlyOnJump(PlayerToggleFlightEvent event) {
        final Player player = event.getPlayer();
        if (player.isOp()) return;
        if (chosen.contains(player.getName())) {
            if (chosensDoubleJumps != 0) {
                if (!jumpCooldown.contains(player.getName())) {
                    if (player.getAllowFlight()) {
                        if (event.isFlying() && event.getPlayer().getGameMode() != GameMode.CREATIVE) {
                            player.setFlying(false);
                            Vector jump = player.getLocation().getDirection().multiply(0.2).setY(1.1);
                            player.setVelocity(player.getVelocity().add(jump));
                            event.setCancelled(true);
                            chosensDoubleJumps--;
                            jumpCooldown.add(player.getName());
                            Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                                @Override
                                public void run() {
                                    jumpCooldown.remove(player.getName());
                                    if (chosen.contains(player.getName())) {
                                        player.sendMessage(prefix + ChatColor.GOLD + "Double jump ready!");
                                        player.setAllowFlight(true);
                                    }
                                }
                            }, 80L);
                        }
                    }
                } else {
                    if (chosen.contains(player.getName())) {
                        player.sendMessage(prefix + ChatColor.RED + "Double jump ability cooling down!");
                        player.setAllowFlight(false);
                    }
                }
            } else {
                player.setAllowFlight(false);
                player.sendMessage(prefix + ChatColor.RED + "You have no double jumps left!");
            }
        } else {
            player.setAllowFlight(false);
        }
    }


    public void awardcoins(int amount, Player player) {
        int coins = getConfig().getInt("Users." + player.getName() + ".coins");
        getConfig().set("Users." + player.getName() + ".coins", coins + amount);
        saveConfig();
        player.sendMessage(" ");
        player.sendMessage(ChatColor.BLUE + "" + ChatColor.BOLD + "-----------------------------");
        player.sendMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "               Reward!");
        player.sendMessage(ChatColor.GOLD + "You have been awarded " + amount + " coins!");
        player.sendMessage(ChatColor.BLUE + "" + ChatColor.BOLD + "-----------------------------");
    }

    public void start() {
        if (Bukkit.getOnlinePlayers().size() >= playersNeeded) {
            if (!started) {
                if (chosen.isEmpty()) {
                    for (Player online : Bukkit.getOnlinePlayers()) {
                        if (!nonochosen.contains(online.getName())) {
                            nonochosen.add(online.getName());
                        }
                    }
                    setFloor(Material.TNT);
                    int random = new Random().nextInt(Bukkit.getOnlinePlayers().size());
                    Player chosenPlayer = (Player) Bukkit.getServer().getOnlinePlayers().toArray()[random];
                    if (nonochosen.contains(chosenPlayer.getName())) {
                        nonochosen.remove(chosenPlayer.getName());
                    }
                    if (!chosen.contains(chosenPlayer.getName())){
                        chosen.add(chosenPlayer.getName());
                    }
                    chosensDoubleJumps = getConfig().getInt("Users." + chosenPlayer.getName() + ".doubleJumps");
                    if (chosensDoubleJumps != 0) {
                        chosenPlayer.setAllowFlight(true);
                    } else {
                        chosensDoubleJumps = 0;
                        chosenPlayer.setAllowFlight(false);
                    }
                    Location chosenLoc = new Location(Bukkit.getWorld("world"), randomeValue(-7, 12), 101, randomeValue(11, 9), 180, 0);
                    chosenPlayer.closeInventory();
                    chosenPlayer.getInventory().clear();
                    chosenPlayer.teleport(chosenLoc);
                    chosenPlayer.sendMessage(prefix + ChatColor.GOLD + "You have been chosen! Try to avoid falling.");
                    getConfig().set("Users." + chosenPlayer.getName() + ".started", new Date());
                    for (String name : nonochosen) {
                        Player player = Bukkit.getPlayer(name);
                        PermissionUser pUser = PermissionsEx.getUser(player);
                        if (pUser.inGroup("ADMIN")) {
                            createItem(player.getInventory(), 7, Material.REDSTONE, 0, ChatColor.GREEN + "Airstrike", new String[]{ChatColor.GRAY + "I think you know what it does..."});
                        }
                        createItem(player.getInventory(), 0, Material.STICK, 0, ChatColor.GREEN + "Gun", new String[]{});
                        createItem(player.getInventory(), 8, Material.EMERALD, 0, ChatColor.GREEN + "Shop", new String[]{ChatColor.GRAY + "Click to open the store!"});
                        if (getConfig().getBoolean("Users." + player.getName() + ".shotgun")) {
                            createItem(player.getInventory(), 1, Material.BLAZE_ROD, 0, ChatColor.GREEN + "Shotgun", new String[]{});
                        }
                    }
                    started = true;
                } else {
                    if (!chosen.isEmpty()) {
                        for (String name : chosen) {
                            Player chosenPlayer = Bukkit.getPlayer(name);
                            Location spawnLoc = new Location(Bukkit.getWorld("world"), 2.5, 115, -30.5, 0, 0);
                            chosenPlayer.teleport(spawnLoc);
                            if (chosen.contains(chosenPlayer.getName())) {
                                chosen.remove(chosenPlayer.getName());
                            }
                            if (!nonochosen.contains(chosenPlayer.getName())) {
                                nonochosen.add(chosenPlayer.getName());
                            }
                        }
                    }
                }
            }
        } else {
            started = false;
            chosen.clear();
            for (Player online : Bukkit.getOnlinePlayers()) {
                online.sendMessage(" ");
                online.sendMessage(prefix + ChatColor.RED + "Not enough players to start! Trying again again in 10 seconds!");
                online.playSound(online.getLocation(), Sound.ENTITY_TNT_PRIMED, 5, 5);
            }
            Bukkit.getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin(this.toString()), new Runnable() {
                @Override
                public void run() {
                    if (!attempting2started) {
                        attemptstart();
                        attempting2started = true;
                    }
                }
            }, 200L);
        }
    }

    public void createItem(Inventory inv, int slot, Material item, int dataint, String displayName, String[] lores) {
        ItemStack i = new ItemStack(item, 1, (byte) dataint);
        ItemMeta m = i.getItemMeta();
        m.setDisplayName(displayName);
        m.setLore(Arrays.asList(lores));
        i.setItemMeta(m);
        inv.setItem(slot, i);
    }


    @EventHandler
    public void onProjectileHitEvent(ProjectileHitEvent event) {
        if (event.getEntityType() == EntityType.ARROW) {
            Player player = (Player) event.getEntity().getShooter();
            final Projectile proj = event.getEntity();
            this.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                @Override
                public void run() {
                    proj.remove();
                }
            }, 40L);
        }
    }

    public final Block getTargetBlock(Player player, int range) {
        BlockIterator iter = new BlockIterator(player, range);
        Block lastBlock = iter.next();
        while (iter.hasNext()) {
            lastBlock = iter.next();
            if (lastBlock.getType() == Material.AIR) {
                continue;
            }
            break;
        }
        return lastBlock;
    }

    @EventHandler
    public void quit(final PlayerQuitEvent event) {
        final Player player = event.getPlayer();
        event.setQuitMessage(null);
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
            @Override
            public void run() {
                if (Bukkit.getOnlinePlayers().size() > 1) {
                    if (chosen.contains(player.getName())) {
                        chosen.remove(player.getName());
                        if (!attempting2started) {
                            attemptstart();
                            attempting2started = true;
                        }
                    }
                } else {
                    if (chosen.contains(player.getName())) {
                        chosen.remove(player.getName());
                        chosen.clear();
                        if (!attempting2started) {
                            started = false;
                            attemptstart();
                            attempting2started = true;
                        }
                    } else {
                        for (Player online : Bukkit.getOnlinePlayers()) {
                            Location spawnLoc = new Location(Bukkit.getWorld("world"), 2.5, 115, -30.5, 0, 0);
                            online.getInventory().clear();
                            online.teleport(spawnLoc);
                            if (!nonochosen.contains(online.getName())) {
                                nonochosen.add(online.getName());
                            }
                            started = false;
                        }
                        chosen.clear();
                    }
                    if (!attempting2started) {
                        started = false;
                        attemptstart();
                        attempting2started = true;
                    }
                }
            }
        }, 5L);
        openConnection();
        try {
            updateDatabaseBool("ChosenShotgun", player, getConfig().getBoolean("Users." + player.getName() + ".shotgun"));
            updateDatabaseInt("ChosenCoins", player, getConfig().getInt("Users." + player.getName() + ".coins"));
            updateDatabaseDouble("ChosenCooldown", player, getConfig().getDouble("Users." + player.getName() + ".cooldown"));
            updateDatabaseBool("ChosenAirstrike", player, getConfig().getBoolean("Users." + player.getName() + ".airstrike"));
            updateDatabaseInt("ChosenDoubleJumps", player, getConfig().getInt("Users." + player.getName() + ".doubleJumps"));
            getConfig().set("Users." + player.getName(), null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }


    @EventHandler
    public void move(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        if (player.getLocation().getY() <= 113) {
            if (nonochosen.contains(player.getName())) {
                if (!chosen.contains(player.getName())) {
                    Location spawnLoc = new Location(Bukkit.getWorld("world"), 2.5, 115, -30.5, 0, 0);
                    player.teleport(spawnLoc);
                    player.sendMessage(prefix + ChatColor.RED + "Don't go jumping off the edge!");
                }
            }
        }
    }

    @EventHandler
    public void interact(PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        if (event.getItem() == null) return;
        if (event.getItem().getType().equals(Material.EMERALD)) {
            openShopInv(event.getPlayer());
        }
        if (event.getItem().getType().equals(Material.STICK)) {
            if (!cooldown.contains(player.getName())) {
                Location loc = player.getEyeLocation().toVector().add(player.getLocation().getDirection().multiply(2)).toLocation(player.getWorld(), player.getLocation().getYaw(), player.getLocation().getPitch());
                Arrow arrow = player.getWorld().spawn(loc, Arrow.class);
                arrow.setShooter(player);
                arrow.setFireTicks(200);
                arrow.setVelocity(player.getEyeLocation().getDirection().multiply(2));
                cooldown.add(player.getName());
                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                    @Override
                    public void run() {
                        if (cooldown.contains(player.getName())) {
                            cooldown.remove(player.getName());
                        }
                    }
                }, (long) getCooldownTicks(getConfig().getString("Users." + player.getName() + ".cooldown")));
            }
        }
        if (event.getItem().getType().equals(Material.BLAZE_ROD)) {
            if (!shotgunCooldown.contains(player.getName())) {
                for (int a = 0; a <= 10; a++) {
                    Location loc1 = player.getLocation();
                    loc1.setPitch((loc1.getPitch() + (getRandom() * 3)));
                    loc1.setYaw((loc1.getYaw() + (getRandom() * 3)));
                    Arrow arrow = player.launchProjectile(Arrow.class);
                    arrow.setFireTicks(200);
                    arrow.setVelocity(loc1.getDirection().multiply(2));
                }
                shotgunCooldown.add(player.getName());
                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                    @Override
                    public void run() {
                        if (shotgunCooldown.contains(player.getName())) {
                            shotgunCooldown.remove(player.getName());
                        }
                    }
                }, 100L);
            }
        }
        if (event.getItem().getType().equals(Material.REDSTONE)) {
            Block targetBlock = getTargetBlock(player, 100);
            if (targetBlock.getType().equals(Material.TNT) || targetBlock.getType().equals(Material.GLOWSTONE)) {
                if (!airstrikeCooldown.contains(player.getName())) {
                    Location loc = targetBlock.getLocation();
                    PermissionUser pUser = PermissionsEx.getUser(player);
                    for (Player online : Bukkit.getOnlinePlayers()) {
                        online.sendMessage(prefix + pUser.getPrefix() + player.getName() + ChatColor.GREEN + " has called a airstrike!");
                    }
                    for (double x = loc.getX() - 3; x < loc.getX() + 6; x++) {
                        for (double z = loc.getZ() - 3; z < loc.getZ() + 6; z++) {
                            Arrow arrow = player.getWorld().spawn(new Location(Bukkit.getWorld("world"), x, 130, z), Arrow.class);
                            arrow.setFireTicks(2000);
                        }
                    }
                    airstrikeCooldown.add(player.getName());
                    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                        @Override
                        public void run() {
                            if (airstrikeCooldown.contains(player.getName())) {
                                airstrikeCooldown.remove(player.getName());
                            }
                        }
                    }, 400L);
                } else {
                    player.sendMessage(prefix + ChatColor.GOLD + "Your airstrike ability cooling down...");
                }
            } else {
                player.sendMessage(prefix + ChatColor.GOLD + "Please look at a area in the arena!");
            }
        }
    }

    public float getRandom(){
        Random r = new Random();
        Random r1 = new Random();
        if(r1.nextBoolean()){
            return r.nextFloat();
        }
        return -r.nextFloat();
    }

    public void setFloor(Material material){
        for (int x = -24; x < 30; x++){
            for (int z = -27; z < 27; z++){
                Bukkit.getWorld("world").getBlockAt(x, 99, z).setType(material, true);
            }
        }
    }
    public static int randomeValue(int max , int min) {
        //Random rand = new Random();
        int ii = -min + (int) (Math.random() * ((max - (-min)) + 1));
        return ii;
    }

    public void openShopInv(Player player){
        inv = Bukkit.createInventory(null, 9*3, ChatColor.DARK_GRAY + "Chosen shop");
        createItem(inv, 11, Material.STICK, 0, ChatColor.YELLOW + "" + ChatColor.BOLD + "Cooldown - " + getConfig().getString("Users." + player.getName() + ".cooldown"), new String[]{ChatColor.GREEN + "Next upgrade: " + getNextCooldownString(getConfig().getString("Users." + player.getName() + ".cooldown")), ChatColor.GOLD + "Price: " + getPriceOfCooldown(getNextCooldownString(getConfig().getString("Users." + player.getName() + ".cooldown")))});
        if (getConfig().getBoolean("Users." + player.getName() + ".shotgun")){
            createItem(inv, 13, Material.BLAZE_ROD, 0, ChatColor.YELLOW + "" + ChatColor.GOLD + "You already have the shotgun!", new String[]{});
        } else {
            createItem(inv, 13, Material.BLAZE_ROD, 0, ChatColor.YELLOW + "" + ChatColor.GOLD + "Buy shotgun", new String[]{ChatColor.GOLD + "Price: " + getPriceOfShotgun(), " ", ChatColor.GRAY + "This can shoot multiple arrows where your", ChatColor.GRAY + "aiming! However its not very accurate."});
        }
        createItem(inv, 18, Material.GOLD_INGOT, 0, ChatColor.GREEN + "Coins: " + getConfig().getInt("Users." + player.getName() + ".coins"), new String[]{});
        createItem(inv, 15, Material.RABBIT_FOOT, 0, ChatColor.YELLOW + "" + ChatColor.BOLD + "Double Jumps - " + getConfig().getString("Users." + player.getName() + ".doubleJumps"), new String[]{ChatColor.GREEN + "Next upgrade: " + getNextDoubleJumpString(getConfig().getString("Users." + player.getName() + ".doubleJumps")), ChatColor.GOLD + "Price: " + getPriceOfDoubleJump(getNextDoubleJumpString(getConfig().getString("Users." + player.getName() + ".doubleJumps")))});
        player.openInventory(inv);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) { //checks for the player to click an item in the GUI created
        final Player player = (Player) e.getWhoClicked();
        if (e.getCurrentItem() == null) return;
        if (e.getCurrentItem().getType().equals(Material.AIR)) return;
        e.setCancelled(true);


        /** Cooldown **/
        if (e.getCurrentItem().getType().equals(Material.STICK)) {
            if (e.getCurrentItem().getItemMeta().getDisplayName().contains("3.0")) {
                updateCooldownConfig(player, 2.5);
            } else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("2.5")){
                updateCooldownConfig(player, 2.0);
            } else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("2.0")) {
                updateCooldownConfig(player, 1.5);
            } else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("1.5")) {
                updateCooldownConfig(player, 1.0);
            } else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("1.0")) {
                updateCooldownConfig(player, 0.7);
            } else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("0.7")){
                updateCooldownConfig(player, 0.5);
            } else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("0.5")){
                updateCooldownConfig(player, 0.3);
            } else {
                player.sendMessage(prefix +ChatColor.GOLD + "Currently we don't want to lower the cooldown anymore the 0.3 of a second.");
            }
            e.setCancelled(true);
        }
        if (e.getCurrentItem().getType().equals(Material.BLAZE_ROD)) {
            if (!getConfig().getBoolean("Users." + player.getName() + ".shotgun")) {
                if (getConfig().getInt("Users." + player.getName() + ".coins") >= 1600) {
                    getConfig().set("Users." + player.getName() + ".shotgun", true);
                    saveConfig();
                    player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 10, 5);
                    createItem(inv, 18, Material.GOLD_INGOT, 0, ChatColor.GREEN + "Coins: " + getConfig().getInt("Users." + player.getName() + ".coins"), new String[]{});
                    createItem(inv, 13, Material.BLAZE_ROD, 0, ChatColor.YELLOW + "" + ChatColor.GOLD + "You already have the shotgun!", new String[]{});
                } else {
                    player.sendMessage(prefix +ChatColor.RED + "You don't have enough coins to purchase this! (" + (getPriceOfCooldown(getNextCooldownString(getConfig().getString("Users." + player.getName() + ".cooldown"))) - 1600));
                }
            }
        }
        if (e.getCurrentItem().getType().equals(Material.RABBIT_FOOT)) {
            if (e.getCurrentItem().getItemMeta().getDisplayName().contains("0")) {
                updateDoubleJumpsConfig(player, 1);
            } else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("1")){
                updateDoubleJumpsConfig(player, 2);
            } else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("2")) {
                updateDoubleJumpsConfig(player, 3);
            } else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("3")) {
                updateDoubleJumpsConfig(player, 4);
            } else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("4")) {
                updateDoubleJumpsConfig(player, 5);
            } else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("5")){
                updateDoubleJumpsConfig(player, 6);
            } else {
                player.sendMessage(prefix +ChatColor.GOLD + "Currently we don't want increase the double jump limit anymore then 6.");
            }
            e.setCancelled(true);
        }
    }

    public void updateCooldownConfig(Player player, double newCooldown){
        if (getConfig().getInt("Users." + player.getName() + ".coins") >= getPriceOfCooldown(getNextCooldownString(getConfig().getString("Users." + player.getName() + ".cooldown")))) {
            int newCoins = (getConfig().getInt("Users." + player.getName() + ".coins") - getPriceOfCooldown(getNextCooldownString(getConfig().getString("Users." + player.getName() + ".cooldown"))));
            getConfig().set("Users." + player.getName() + ".coins", newCoins);
            getConfig().set("Users." + player.getName() + ".cooldown", newCooldown);
            saveConfig();
            player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 10, 5);
            createItem(inv, 18, Material.GOLD_INGOT, 0, ChatColor.GREEN + "Coins: " + getConfig().getInt("Users." + player.getName() + ".coins"), new String[]{});
            createItem(inv, 11, Material.STICK, 0, ChatColor.YELLOW + "" + ChatColor.BOLD + "Cooldown - " + getConfig().getString("Users." + player.getName() + ".cooldown"), new String[]{ChatColor.GREEN + "Next upgrade: " + getNextCooldownString(getConfig().getString("Users." + player.getName() + ".cooldown")), ChatColor.GOLD + "Price: " + getPriceOfCooldown(getNextCooldownString(getConfig().getString("Users." + player.getName() + ".cooldown")))});
        } else {
            player.sendMessage(prefix +ChatColor.RED + "You don't have enough coins to purchase this! (" + (getPriceOfCooldown(getNextCooldownString(getConfig().getString("Users." + player.getName() + ".cooldown"))) - getConfig().getInt("Users." + player.getName() + ".coins")) + " more coins needed)");
        }
    }

    public void updateDoubleJumpsConfig(Player player, int newJumps){
        if (getConfig().getInt("Users." + player.getName() + ".coins") >= getPriceOfDoubleJump(getNextDoubleJumpString(getConfig().getString("Users." + player.getName() + ".doubleJumps")))) {
            int newCoins = (getConfig().getInt("Users." + player.getName() + ".coins") - getPriceOfDoubleJump(getNextDoubleJumpString(getConfig().getString("Users." + player.getName() + ".doubleJumps"))));
            getConfig().set("Users." + player.getName() + ".coins", newCoins);
            getConfig().set("Users." + player.getName() + ".doubleJumps", newJumps);
            saveConfig();
            player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 10, 5);
            createItem(inv, 18, Material.GOLD_INGOT, 0, ChatColor.GREEN + "Coins: " + getConfig().getInt("Users." + player.getName() + ".coins"), new String[]{});
            createItem(inv, 15, Material.RABBIT_FOOT, 0, ChatColor.YELLOW + "" + ChatColor.BOLD + "Double Jumps - " + getConfig().getString("Users." + player.getName() + ".doubleJumps"), new String[]{ChatColor.GREEN + "Next upgrade: " + getNextDoubleJumpString(getConfig().getString("Users." + player.getName() + ".doubleJumps")), ChatColor.GOLD + "Price: " + getPriceOfDoubleJump(getNextDoubleJumpString(getConfig().getString("Users." + player.getName() + ".doubleJumps")))});
        } else {
            player.sendMessage(prefix +ChatColor.RED + "You don't have enough coins to purchase this! (" + (getPriceOfDoubleJump(getNextDoubleJumpString(getConfig().getString("Users." + player.getName() + ".doubleJumps"))) - getConfig().getInt("Users." + player.getName() + ".coins")) + " more coins needed)");
        }
    }

    public synchronized void openConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost/minecraft", "root", "password");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onplayerlogin(final PlayerLoginEvent event) {
        Player player = event.getPlayer();
        openConnection();
        try {
            getConfig().set("Users." + player.getName() + ".coins", getDatabaseInt("ChosenCoins", player));
            saveConfig();
            getConfig().set("Users." + player.getName() + ".cooldown", getDatabaseDouble("ChosenCooldown", player));
            saveConfig();
            getConfig().set("Users." + player.getName() + ".shotgun", getDatabaseBool("ChosenShotgun", player));
            saveConfig();
            getConfig().set("Users." + player.getName() + ".airstrike", getDatabaseBool("ChosenAirstrike", player));
            saveConfig();
            getConfig().set("Users." + player.getName() + ".doubleJumps", getDatabaseInt("ChosenDoubleJumps", player));
            saveConfig();
            Server server = Bukkit.getServer();
            ConsoleCommandSender console = server.getConsoleSender();
            console.sendMessage(prefix +ChatColor.YELLOW + player.getName() + "'s " + ChatColor.GOLD + "chosen cache has been loaded!");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    public boolean getDatabaseBool(String select, Player player) {
        try {
            PreparedStatement sqlcoins = connection.prepareStatement("SELECT " + select + " FROM `player_data` WHERE Name=?;");
            sqlcoins.setString(1, player.getName());
            ResultSet resultcoins = sqlcoins.executeQuery();
            resultcoins.next();
            Boolean bool = resultcoins.getBoolean(select);
            sqlcoins.close();
            resultcoins.close();
            return bool;
        } catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public int getDatabaseInt(String select, Player player) {
        try {
            PreparedStatement sqlcoins = connection.prepareStatement("SELECT " + select + " FROM `player_data` WHERE Name=?;");
            sqlcoins.setString(1, player.getName());
            ResultSet resultcoins = sqlcoins.executeQuery();
            resultcoins.next();
            int inta = resultcoins.getInt(select);
            sqlcoins.close();
            resultcoins.close();
            return inta;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public double getDatabaseDouble(String select, Player player) {
        try {
            PreparedStatement sqlcoins = connection.prepareStatement("SELECT " + select + " FROM `player_data` WHERE Name=?;");
            sqlcoins.setString(1, player.getName());
            ResultSet resultcoins = sqlcoins.executeQuery();
            resultcoins.next();
            double inta = resultcoins.getDouble(select);
            sqlcoins.close();
            resultcoins.close();
            return inta;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0.0;
    }

    public void updateDatabaseInt(String select, Player player, int amount){
        try {
            PreparedStatement sql = connection.prepareStatement("SELECT " + select + " FROM `player_data` WHERE Name=?;");
            sql.setString(1, player.getName());
            PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `player_data` SET " + select + "=? WHERE Name=?");
            loginsupdate.setInt(1, amount);
            loginsupdate.setString(2, player.getName());
            loginsupdate.executeUpdate();
            loginsupdate.close();
            sql.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateDatabaseBool(String select, Player player, Boolean bool){
        try {
            PreparedStatement sql = connection.prepareStatement("SELECT " + select + " FROM `player_data` WHERE Name=?;");
            sql.setString(1, player.getName());
            PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `player_data` SET " + select + "=? WHERE Name=?");
            loginsupdate.setBoolean(1, bool);
            loginsupdate.setString(2, player.getName());
            loginsupdate.executeUpdate();
            loginsupdate.close();
            sql.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateDatabaseDouble(String select, Player player, double amount){
        try {
            PreparedStatement sql = connection.prepareStatement("SELECT " + select + " FROM `player_data` WHERE Name=?;");
            sql.setString(1, player.getName());
            PreparedStatement loginsupdate = connection.prepareStatement("UPDATE `player_data` SET " + select + "=? WHERE Name=?");
            loginsupdate.setDouble(1, amount);
            loginsupdate.setString(2, player.getName());
            loginsupdate.executeUpdate();
            loginsupdate.close();
            sql.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        if (date1 != null) {
            if (date2 != null) {
                diffInMillies = date2.getTime() - date1.getTime();
            }
        }
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }


    public String getNextCooldownString(String cooldown){
        String next = null;
        switch (cooldown) {
            case "3.0":  next = "2.5";
                break;
            case "2.5": next = "2.0";
                break;
            case "2.0": next = "1.5";
                break;
            case "1.5": next = "1.0";
                break;
            case "1.0": next = "0.7";
                break;
            case "0.7": next = "0.5";
                break;
            case "0.5": next = "0.3";
                break;
            case "0.3": next = "MAXED OUT";
                break;
        }
        return next;
    }
    public String getNextDoubleJumpString(String jumps){
        String next = null;
        switch (jumps) {
            case "0": next = "1";
                break;
            case "1":
                next = "2";
                break;
            case "2":
                next = "3";
                break;
            case "3":
                next = "4";
                break;
            case "4":
                next = "5";
                break;
            case "5":
                next = "6";
                break;
            case "6":
                next = "MAXED OUT";
                break;
        }
        return next;
    }

    public int getPriceOfDoubleJump(String jumps){
        int price = 0;
        switch (jumps) {
            case "1":  price = 50;
                break;
            case "2":  price = 50;
                break;
            case "3":  price = 50;
                break;
            case "4":  price = 50;
                break;
            case "5":  price = 50;
                break;
            case "6":  price = 50;
                break;
        }
        return price;
    }

    public int getCooldownTicks(String cooldown){
        int ticks = 0;
        switch (cooldown) {
            case "3.0":  ticks = 60;
                break;
            case "2.5": ticks = 50;
                break;
            case "2.0": ticks = 40;
                break;
            case "1.5": ticks = 30;
                break;
            case "1.0": ticks = 20;
                break;
            case "0.7": ticks = 17;
                break;
            case "0.5": ticks = 10;
                break;
            case "0.3": ticks = 4;
                break;
        }
        return ticks;
    }

    public int getPriceOfCooldown(String cooldown){
        int price = 0;
        switch (cooldown) {
            case "2.5":  price = 20;
                break;
            case "2.0":  price = 40;
                break;
            case "1.5":  price = 160;
                break;
            case "1.0":  price = 380;
                break;
            case "0.7":  price = 500;
                break;
            case "0.5":  price = 620;
                break;
            case "0.3":  price = 840;
                break;
        }
        return price;
    }

    public int getPriceOfShotgun(){
        return 1600;
    }

    @EventHandler
    public void healthChange(FoodLevelChangeEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void playerDamage(EntityDamageEvent entityDamageEvent){
        entityDamageEvent.setCancelled(true);
    }

    @EventHandler
    public void itemDrop(PlayerDropItemEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void tntExplode(EntityExplodeEvent explodeEvent){
        explodeEvent.setCancelled(true);
    }
    @EventHandler
    public void weather(WeatherChangeEvent event){
        event.setCancelled(true);
    }

}
